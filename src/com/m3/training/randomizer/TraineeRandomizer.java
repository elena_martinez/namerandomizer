package com.m3.training.randomizer;

import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.m3.training.listgen.ListGenFactory;
import com.m3.training.listgen.ProcessArgs;

public class TraineeRandomizer {
	
	public static final String USAGE = 
			"Please enter a cofiguration file mode (ListFromXML, ListFromProperties, ListFromJson) and a configuration file.";
	public static final String log4jConfigFile = "resources/log4j.properties";
	
	public static void main(String[] args) {
		/* Let us use logging. Decouple publisher from subscribers. */
        PropertyConfigurator.configure(log4jConfigFile);
		Logger logger = Logger.getLogger(TraineeRandomizer.class);
		String msg;
		
		if (args == null || args.length < 2) {
			msg = USAGE;
			logger.error(msg);
		}

		String fileMode = args[0];
		// Process any of an xml config file or a properties config file or a JSON config file
		ProcessArgs listGen = ListGenFactory.createListGen(fileMode);
		
		String fileName = args[1];
		String[] fileNameArray = new String[1];
		fileNameArray[0] = fileName;
		
		List<String> names = listGen.processArgs(fileNameArray);
		msg = "Team members: " + names.size(); 
		Collections.sort(names);
		logger.info(msg);
		msg = "";
		int count = names.size();
		int current = 1;
		String thisPiece = "";
		for (String name : names) {
			thisPiece += name;
			if (count != current++) {
				thisPiece += ", ";
			}
			if (thisPiece.length() >= 80) {
				msg += thisPiece;
				msg += "\n";
				thisPiece = "";
			}
		}
		msg += thisPiece;
		logger.info(msg);
		PrintOrdering.printWithPause(names);	
	}
	
	
}
