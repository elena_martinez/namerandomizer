package com.m3.training.randomizer;

import java.util.Collections;
import java.util.List;
import java.util.Scanner;

import org.apache.log4j.Logger;

public class PrintOrdering {
	
	public static void printWithPause(List<String> names) {
		Logger logger = Logger.getRootLogger();
		Scanner scanner = new Scanner(System.in);
		while (names.size() > 0) {
			Collections.shuffle(names);
			logger.info("Selection: " + names.remove(0));
			if (names.size() > 0) {
				if (names.size() == 1) {
					logger.info("\nFinally: " + names.remove(0));
				}
				else {
					scanner.nextLine();
				}
			}
		}
		scanner.close();
	}
}
