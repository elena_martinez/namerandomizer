package com.m3.training.listgen;

import java.io.FileNotFoundException;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class ListFromJson implements ProcessArgs {
	
	final static Logger logger = Logger.getLogger(ListFromJson.class);

	private List<String> buildList(String filename) throws IOException {
		ArrayList<String> names = new ArrayList<>();
		String content = new String(Files.readAllBytes(Paths.get(filename)));
		JSONObject obj = new JSONObject(content);
		JSONArray arr = obj.getJSONArray("trainees");
		for (int traineeIndex = 0; traineeIndex < arr.length(); traineeIndex++) {
		    String name = arr.getJSONObject(traineeIndex).getString("first_name");
		    names.add(name);
		}
		return names;
	}

	public List<String> processArgs(String[] args) {
		List<String> names = new ArrayList<>();
		String msg = "Please enter a JSON file name.";
		if (args.length < 1) {
			logger.error(msg);
			return names;
		}
		String filename = args[0];
		try {
			names = buildList(filename);
		} catch (FileNotFoundException e) {
			msg = "The requested file could not be found: [" + filename + "]";
			logger.error(msg);
		} catch (IOException e) {
			msg = "The requested file could not be read: [" + filename + "]";
			logger.error(msg);
		} catch (org.json.JSONException e) {
			msg = "The requested file contains invalid JSON data: [" + filename + "]";
			logger.error(msg);
		}
		if (names.size() == 0) {
			msg = "The requested file contains no data: [" + filename + "]";
			logger.error(msg);
		}
		return names;
	}

}
