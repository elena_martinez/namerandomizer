package com.m3.training.listgen;

import java.util.List;

public interface ProcessArgs {
	public List<String> processArgs(String[] args);
}
