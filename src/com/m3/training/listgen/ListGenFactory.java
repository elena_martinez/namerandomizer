package com.m3.training.listgen;

import java.lang.reflect.InvocationTargetException;

public class ListGenFactory {
	
	public final static  String FACTORY_ERROR = "Could not instantiate list generator.";

	public static ProcessArgs createListGen(String name) {
		Object pa = null;
		Class<?> clazz;
		String msg = FACTORY_ERROR + " [" + name + "]";
		
		// try to get a class from the provided name
		try {
			clazz = Class.forName(name);
			// make an array that represents the classes of the parameters to the constructor: none
			Class<?>[] classesOfParameters = null;
			
			// make an array that represents the parameters: none
			Object[] args = null;
			
			// make the instance
			pa = clazz.getConstructor(classesOfParameters).newInstance(args);
			
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
			throw new IllegalStateException(msg, e);
		}
		
		// The requested class must implement ProcessArgs
		if (ProcessArgs.class.isInstance(pa)) {
			return (ProcessArgs) pa;
		}
		else {
			throw new IllegalStateException(msg);
		}
	}

}
