package com.m3.training.listgen;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ListFromXML implements ProcessArgs {
	
	final static Logger logger = Logger.getLogger(ListFromJson.class);

	private List<String> buildList(String filename) throws FileNotFoundException, IOException, ParserConfigurationException, SAXException {
		List<String> names = new ArrayList<>();
		try (InputStream inStream = new FileInputStream(filename)) {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document document = builder.parse(new File(filename));
			NodeList nodeList = document.getDocumentElement().getChildNodes();
			for (int nodeIndex = 0; nodeIndex < nodeList.getLength(); nodeIndex++) {
				Node node = nodeList.item(nodeIndex);
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					Element elem = (Element) node;
					NodeList traineeList = elem.getElementsByTagName("trainee");
					for (int traineeIndex = 0; traineeIndex < traineeList.getLength(); traineeIndex++) {
						Node traineeNode = traineeList.item(traineeIndex);
						String fn = traineeNode.getAttributes().getNamedItem("firstName").getNodeValue();
						String ln = traineeNode.getAttributes().getNamedItem("lastName").getNodeValue();
						names.add(fn + " " + ln);
					}
				}
			}
			return names;
		}
	}

	public List<String> processArgs(String[] args) {
		List<String> names = new ArrayList<>();
		String msg;
		if (args.length < 1) {
			msg = "Please enter an xml file name.";
			logger.error(msg);
			return names;
		}
		String filename = args[0];
		try {
			names = buildList(filename);
		} catch (FileNotFoundException e) {
			msg = "The requested file could not be found: [" + filename + "]";
			logger.error(msg);
		} catch (IOException e) {
			msg = "The requested file could not be read: [" + filename + "]";
			logger.error(msg);
		} catch (ParserConfigurationException | SAXException e) {
			msg = "The requested file could not be parsed: [" + filename + "]";
			logger.error(msg);
		}	
		return names;
	}

}
